FROM ubuntu:16.04
COPY fuzzing/ /usr/src/
USER root
RUN apt-get update && apt-get install -y \
  sudo \
  software-properties-common \
  build-essential