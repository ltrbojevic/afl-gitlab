#!/usr/bin/python3
import subprocess
import time
import os

def main():

    subprocess.Popen(["afl-fuzz", "-i", "inputs", "-o", "outputs", "./vulnerable"])
    time.sleep(30)
    subprocess.Popen(["pkill", "-f", "afl"])
    os._exit(0)

if __name__ == '__main__':
    main()