#include <stdio.h>
#include <string.h>

int main(void)
{
    char SocialSecurityNumber[25];
    char BankAccountNumber[25];
    char RoutingNumber[25];

    printf("Please enter your Social Security Number: \n");
    gets(SocialSecurityNumber);
    printf("Please enter your Bank Account number: \n");
    gets(BankAccountNumber);
    printf("Please enter your Routing number: \n");
    gets(RoutingNumber);

    if (strcmp(SocialSecurityNumber, "XXX-XX-XXXX") != NULL) {
        if (strcmp(BankAccountNumber, "XXXXXXXXXXX") != NULL) {
            if (strcmp(RoutingNumber, "XXXXXX") != NULL) {
                return 0;
            }
        }
    }

    return 1;
}