## About this project

The purpose of this project is to provide an example of how you can automate instrumented fuzzing with AFL using GitLab pipelines. It provides a sample Dockerfile, an example C program to be fuzzed, and a `.gitlab-ci.yml` file configuring the pipeline to run AFL. 

This very simple, hands-on example is intended to serve as a starting point that pulls together all the moving pieces and explains how they work together, and to then be extended for more advanced usage. This particular example uses AFL, though practically any fuzzer can be used. The core concepts of how this works with AFL applies to just about any other fuzzer or tool. 

The exact Docker image I used is in [this](https://cloud.docker.com/u/ltrbojevic/repository/docker/ltrbojevic/afl-gitlab) public DockerHub repository.

## The components

`Dockerfile`:
* Instructions for building the Docker image. It copies your local files into the Docker image and installs necessary dependencies.

`.gitlab-ci.yml`:
* Defines your GitLab pipeline. It:
  * Declares which Docker image is being used
  * Installs AFL
  * Instruments the target
  * Runs `afl-gitlab.py`, which initiates and stops AFL
  * Collects the output as an [artifact](https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html).

`/afl-gitlab`:
* Contains the example target (in this case, `vulnerable.c` and its Makefile)
  * It's just a simple C program where mostly everything that can be done wrong, is. We intentionally want to find a unique crash quickly for demonstrative purposes.
* Contains `afl-gitlab.py`, which initiates and stops AFL
* Contains the fuzzer inputs in `afl-gitlab/inputs`

## How it all works

### Build your Docker image

#### Dockerfile

The [Dockerfile](https://docs.docker.com/engine/reference/builder/) provides the instructions for Docker to build a Docker image. 

In this example, we're using an Ubuntu 16.04 base image:

`FROM ubuntu:16.04`

We're then copying the `fuzzing` directory to `/usr/src/`:

`COPY fuzzing/ /usr/src/`

Running commands as `root` because this was done in a time crunch and everything just needs to work:

`USER root`

Then we install the packages we need:

```RUN apt-get update && apt-get install -y \
  sudo \
  software-properties-common \
  build-essential
  ```
  
#### Build the image and push to DockerHub

To serve as a simple example, to build and push your image to DockerHub:

`docker build -t [name of your image] .`

`docker tag [name of your image] [username]/[repository name]:[tag]`

`docker push [username]/[repository name]:[tag]`

For example, I ran:

`docker build -t afl-gitlab-test .`

`docker tag afl-gitlab-test ltrbojevic/afl-gitlab:latest`

`docker push ltrbojevic/afl-gitlab:latest`
  
### Setting up and running your pipeline

Detailed documentation on pipelines are on the GitLab [website](https://docs.gitlab.com/ee/ci/pipelines.html).

For this example, assuming it's being run on GitLab.com:

* Go to your project settings > CI/CD Settings and expand the `Runners` section. Make sure `Shared Runners` are enabled. If they aren't, enable them.
* Add the `.gitlab.ci-yml` to your repository.
* Start the pipeline

### A note on performance

[Runner](https://docs.gitlab.com/runner/) runs pipeline jobs and sends the result back to GitLab.

#### Self-hosted vs Shared Runners

This specific example uses GitLab.com [Shared Runners](https://docs.gitlab.com/ee/ci/runners/#shared-specific-and-group-runners) for demonstration’s sake. The performance limitations of Shared Runners in the context of fuzzing make it infeasible to run instrumented fuzzing jobs in a performant way. Instead, consider using a self-hosted Runner.

#### Selecting your Executor

This specific example uses the [Docker executor](https://docs.gitlab.com/runner/executors/docker.html) because I prefer to work with Docker images. There are [many other](https://docs.gitlab.com/runner/executors/README.html) executors you can use depending on your requirements.